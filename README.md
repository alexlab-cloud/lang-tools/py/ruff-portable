# ruff-portable

> An experiment attempting to create installable (and reusable) [Ruff][links.ruff] configs

Tools like [Prettier](https://prettier.io/) and [ESLint](https://eslint.org/) allow you to install custom configurations
as packages. This makes automating dev tooling a trivial task between projects, and keeps things consistent. This
project is an attempt at installing a `ruff.toml` as a Python package from a GitLab registry.

<hr>

<sub>Emoji used for repository logo designed by <a href="https://openmoji.org/">OpenMoji</a> – the open-source emoji and icon project. License: CC BY-SA 4.0</sub>

<hr>

<!-- Links -->
[links.ruff]: https://docs.astral.sh/ruff/
